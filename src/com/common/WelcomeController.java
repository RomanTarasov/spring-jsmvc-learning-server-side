package com.common;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tti.model.Greeting;
import com.tti.model.Task;

@Controller
public class WelcomeController {
 
	private static final String VIEW_INDEX = "index";
	private final static Logger logger = LoggerFactory.getLogger(WelcomeController.class);
 
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(ModelMap model) {
 
		model.addAttribute("message", "");
		logger.debug("[welcome]");
 
		return VIEW_INDEX;
 
	}
 
	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public String welcomeName(@PathVariable String name, ModelMap model) {
 
		model.addAttribute("message", "Welcome " + name);
		logger.debug("[welcomeName] name : {}", name);
		return VIEW_INDEX;
 
	}
		
	@RequestMapping(value = "/task/create", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
	public @ResponseBody Task createTask(@RequestBody Task model) {//@PathVariable String description) {//@RequestBody Task model) {
				
		logger.debug("[task.create] task description : {}", model.getDescription());
		
		model.setId("1000");
		
		logger.debug("[task.create] new task id : {}", model.getId());
		
		model.setNote(model.getId() + " note");
		
		model.setHours("10");
		
		return model;
 
	}
	
	
	@RequestMapping(value = "/testpost", method = RequestMethod.POST, produces = {"application/json"})
    public @ResponseBody Greeting helloWorldNew() {
        String message = "Hello World_new, Spring 3.0!";
        System.out.println(message);
        return new Greeting("asdf");
    }
	
 
}