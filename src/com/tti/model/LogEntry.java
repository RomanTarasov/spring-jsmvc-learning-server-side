package com.tti.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="logEntries")
public class LogEntry implements TtiModel {
	
	@Id
	private String id;
	private String description;
	private String note;
	private String hours;
	@DBRef
	private Task task;
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public void setId(String id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getHours() {
		return hours;
	}
	
	public void setHours(String hours) {
		this.hours = hours;
	}
	
	public Task getTask() {
		return task;
	}
	
	public void setTask(Task task) {
		this.task = task;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder("LogEntry(");
		builder.append("id: ");
		builder.append(id);
		builder.append(", description: ");
		builder.append(description);
		builder.append(", note: ");
		builder.append(note);
		builder.append(", hours: ");
		builder.append(hours);
		builder.append(")");
		return builder.toString();
	}
	
}
