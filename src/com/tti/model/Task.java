package com.tti.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="tasks")
public class Task implements TtiModel {
	
	@Id
	private String id;
	private String description;
	private String note;
	private String hours;
	private String name;
	private String clientId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String toString() {
		StringBuilder builder = new StringBuilder("Task(");
		builder.append("id: ");
		builder.append(id);
		builder.append(", description: ");
		builder.append(description);
		builder.append(", note: ");
		builder.append(note);
		builder.append(", name: ");
		builder.append(name);
		builder.append(")");
		return builder.toString();
	}
	
	
	

}
