package com.tti.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="clients")
public class Client implements TtiModel {
	
	@Id
	private String id;
	private String name;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder("Client(");
		builder.append("id: ");
		builder.append(id);
		builder.append(", name: ");
		builder.append(name);
		builder.append(")");
		return builder.toString();
	}
	
	
	
	

}