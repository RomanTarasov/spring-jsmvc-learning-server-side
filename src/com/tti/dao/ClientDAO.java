package com.tti.dao;

import org.springframework.stereotype.Repository;

import com.tti.model.Client;

@Repository
public interface ClientDAO extends TtiDAO<Client> {


}
