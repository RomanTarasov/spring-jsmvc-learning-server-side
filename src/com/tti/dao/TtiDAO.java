package com.tti.dao;

import java.util.List;

public interface TtiDAO<T> {
	public List<T> getAll();
	public T save(String id, T subject);
	public T findById(String id);
	public T removeById(String id);
	public List<T> findByDescription(String name);

}
