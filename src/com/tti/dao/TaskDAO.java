package com.tti.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tti.model.Task;

@Repository
public interface TaskDAO extends TtiDAO<Task>{
	public List<Task> findTasksByClientId(String id);
}
