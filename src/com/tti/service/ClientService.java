package com.tti.service;

import java.util.List;

import com.tti.model.Client;

public interface ClientService {
	public List<Client> getAllClients();
	public Client findClientById(String id);
	public Client saveClient(String id, Client client);
	public Client removeClientById(String id);
	public List<Client> findByDescription(String description);
}
