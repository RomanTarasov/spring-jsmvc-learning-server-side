package com.tti.service.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tti.dao.TaskDAO;
import com.tti.model.Task;
import com.tti.service.TaskService;

@Service
public class TaskServiceFacade implements TaskService {
	
	@Autowired
	TaskDAO taskDAO;

	@Override
	public List<Task> getAllTasks() {
		return taskDAO.getAll();
	}

	@Override
	public Task findTasksByClientId(String id) {
		return taskDAO.findById(id);
	}

	@Override
	public Task saveTask(String id, Task task) {
		return taskDAO.save(id, task);
	}

	@Override
	public Task removeTaskById(String id) {
		return taskDAO.removeById(id);
	}

	@Override
	public List<Task> findByDescription(String description) {
		return taskDAO.findByDescription(description);
	}

	@Override
	public List<Task> getTasksByClientId(String id) {		
		return taskDAO.findTasksByClientId(id);
	}

}
