package com.tti.service.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tti.dao.ClientDAO;
import com.tti.model.Client;
import com.tti.service.ClientService;

@Service
public class ClientServiceFacade implements ClientService {
	
	@Autowired
	ClientDAO clientDAO;

	@Override
	public List<Client> getAllClients() {
		return clientDAO.getAll();
	}

	@Override
	public Client findClientById(String id) {
		return clientDAO.findById(id);
	}

	@Override
	public Client saveClient(String id, Client client) {
		return clientDAO.save(id, client);
	}

	@Override
	public Client removeClientById(String id) {
		return clientDAO.removeById(id);
	}

	@Override
	public List<Client> findByDescription(String description) {
		return clientDAO.findByDescription(description);
	}

}
