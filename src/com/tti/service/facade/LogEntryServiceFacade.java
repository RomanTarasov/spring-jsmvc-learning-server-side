package com.tti.service.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tti.dao.LogEntryDAO;
import com.tti.model.LogEntry;
import com.tti.service.LogEntryService;

@Service
public class LogEntryServiceFacade implements LogEntryService {
	
	@Autowired
	LogEntryDAO logEntryDAO;

	@Override
	public List<LogEntry> getAllLogEntries() {
		return logEntryDAO.getAll();
	}

	@Override
	public LogEntry findLogEntryById(String id) {
		return logEntryDAO.findById(id);
	}

	@Override
	public LogEntry saveLogEntry(String id, LogEntry logEntry) {
		return logEntryDAO.save(id, logEntry);
	}

	@Override
	public LogEntry removeLogEntryById(String id) {
		return logEntryDAO.removeById(id);
	}

	@Override
	public List<LogEntry> findByDescription(String description) {
		return logEntryDAO.findByDescription(description);
	}

}
