package com.tti.service;

import java.util.List;

import com.tti.model.Task;

public interface TaskService {
	public List<Task> getAllTasks();
	public Task findTasksByClientId(String id);
	public Task saveTask(String id, Task task);
	public Task removeTaskById(String id);
	public List<Task> findByDescription(String description);
	public List<Task> getTasksByClientId(String id);
}
