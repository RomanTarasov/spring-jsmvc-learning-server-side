package com.tti.service;

import java.util.List;

import com.tti.model.LogEntry;

public interface LogEntryService {
	public List<LogEntry> getAllLogEntries();
	public LogEntry findLogEntryById(String id);
	public LogEntry saveLogEntry(String id, LogEntry logEntry);
	public LogEntry removeLogEntryById(String id);
	public List<LogEntry> findByDescription(String description);

}
