package com.tti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tti.model.Task;
import com.tti.service.TaskService;

@Controller
@RequestMapping("/tasks")
public class TaskController {
	@Autowired
	TaskService taskFacade;
	
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Task> getTasks(){		
		return taskFacade.getAllTasks();
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public @ResponseBody Task findDocument(@PathVariable String id){
		return taskFacade.findTasksByClientId(id);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody Task addTask(@RequestBody Task task){
		String id = task.getId();
		return taskFacade.saveTask(id,task);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.PUT)
	public @ResponseBody Task updateTask(@RequestBody Task task, @PathVariable String id){
		return taskFacade.saveTask(id,task);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.DELETE)
	public @ResponseBody Task removeDocument(@PathVariable String id){
		return taskFacade.removeTaskById(id);
	}
	
	@RequestMapping(value="/client/{id}",method=RequestMethod.POST)
	public @ResponseBody List<Task> tasksByClient(@PathVariable String id){
		return taskFacade.getTasksByClientId(id);
	}
	
}
