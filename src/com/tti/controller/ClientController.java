package com.tti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tti.model.Client;
import com.tti.service.ClientService;

@Controller
@RequestMapping("/clients")
public class ClientController {
	
	@Autowired
	ClientService clientFacade;
	
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Client> getClients(){		
		return clientFacade.getAllClients();
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public @ResponseBody Client findDocument(@PathVariable String id){
		return clientFacade.findClientById(id);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody Client addClient(@RequestBody Client client){
		String id = client.getId();
		return clientFacade.saveClient(id, client);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.PUT)
	public @ResponseBody Client updateClient(@RequestBody Client client, @PathVariable String id){
		return clientFacade.saveClient(id, client);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.DELETE)
	public @ResponseBody Client removeDocument(@PathVariable String id){
		return clientFacade.removeClientById(id);
	}
}