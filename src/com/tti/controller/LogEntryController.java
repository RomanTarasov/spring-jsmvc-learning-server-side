package com.tti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tti.model.LogEntry;
import com.tti.service.LogEntryService;

@Controller
@RequestMapping("/logEntries")
public class LogEntryController {
	@Autowired
	LogEntryService logEntryFacade;
	
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<LogEntry> getTasks(){		
		return logEntryFacade.getAllLogEntries();
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public @ResponseBody LogEntry findDocument(@PathVariable String id){
		return logEntryFacade.findLogEntryById(id);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public @ResponseBody LogEntry addTask(@RequestBody LogEntry logEntry){
		String id = logEntry.getId();
		return logEntryFacade.saveLogEntry(id,logEntry);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.PUT)
	public @ResponseBody LogEntry updateTask(@RequestBody LogEntry logEntry, @PathVariable String id){
		return logEntryFacade.saveLogEntry(id,logEntry);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.DELETE)
	public @ResponseBody LogEntry removeDocument(@PathVariable String id){
		return logEntryFacade.removeLogEntryById(id);
	}

}
