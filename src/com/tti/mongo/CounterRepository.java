package com.tti.mongo;

//import static org.springframework.data.mongodb.core.query.Query.*;
//import static org.springframework.data.mongodb.core.query.Criteria.*;
//import static org.springframework.data.mongodb.core.FindAndModifyOptions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.tti.model.Counter;
import com.tti.exception.SequenceException;

@Service
public class CounterRepository {
	@Autowired private MongoOperations mongo;
	   
	  public int getNextSequence(String collectionName) throws SequenceException {
		  /*
	    Counter counter = mongo.findAndModify(
	      query(where("id").is(collectionName)), 
	      new Update().inc("seq", 1),
	      options().returnNew(true),
	      Counter.class,
	      "counters");
	       */
		  Query query = new Query(Criteria.where("id").is(collectionName));
		
		  Update update = new Update();
		  update.inc("seq", 1);
	 
		  //return new increased id
		  FindAndModifyOptions options = new FindAndModifyOptions();
		  options.returnNew(true);
		  options.upsert(true);
		  	 
		  //this is the magic happened.
		  Counter seqId = mongo.findAndModify(query, update, options, Counter.class);
	 
		  //if no id, throws SequenceException
	          //optional, just a way to tell user when the sequence id is failed to generate.
		  if (seqId == null) {
			throw new SequenceException("Unable to get sequence id for key : " + collectionName);
		  }
	 
		  return seqId.getSeq();		
	  }
}
