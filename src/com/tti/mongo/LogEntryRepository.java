package com.tti.mongo;

import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.tti.dao.LogEntryDAO;
import com.tti.model.LogEntry;

@Repository("mongoLogEntryDAO")
public class LogEntryRepository extends TtiRepository<LogEntry> implements LogEntryDAO {

	@Override
	protected void populateUpdate(Update update, LogEntry subject, LogEntry subjectUpdate) {
		update.set("description",null == subject.getDescription() ? subjectUpdate.getDescription():subject.getDescription());
		update.set("note",null == subject.getNote() ? subjectUpdate.getNote():subject.getNote());
		update.set("hours",null == subject.getHours() ? subjectUpdate.getHours() : subject.getHours());		
	}

}
