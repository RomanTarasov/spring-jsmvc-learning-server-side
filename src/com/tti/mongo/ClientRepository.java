package com.tti.mongo;

import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.tti.dao.ClientDAO;
import com.tti.model.Client;

@Repository("mongoClientDAO")
public class ClientRepository extends TtiRepository<Client> implements ClientDAO {

	@Override
	protected void populateUpdate(Update update, Client subject, Client subjectUpdate) {
		update.set("name",null == subject.getName() ? subjectUpdate.getName():subject.getName());		
	}


}
