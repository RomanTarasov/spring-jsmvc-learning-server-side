package com.tti.mongo;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.tti.dao.TtiDAO;
import com.tti.model.TtiModel;

public abstract class TtiRepository<T extends TtiModel> implements TtiDAO<T> {
	
	private Class<T> entityType;
	
	@Autowired
	private MongoOperations mongoTemplate;
	
	@Autowired
	private CounterRepository counterService;
	
	@SuppressWarnings("unchecked")
	public TtiRepository() {
		this.entityType = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);		
	}

	@Override
	public List<T> getAll() {
		return mongoTemplate.findAll(this.entityType);
	}

	@Override
	public T findById(String id) {
		Query query = query(where("id").is(id));
		return mongoTemplate.findOne(query, this.entityType);
	}
	
	public T save(String id, T subject) {
		T _subjectUpdate = null;
		if(id == null || "".equals(id)) {
			int nextId = counterService.getNextSequence(this.entityType.getSimpleName());
			subject.setId(String.valueOf(nextId));
		} else {
			_subjectUpdate = findById(id);
		}
		
		if(null==_subjectUpdate){
			mongoTemplate.insert(subject);
		}else{
			Query query = query(where("id").is(id));
			Update update = new Update();
			
			this.populateUpdate(update, subject, _subjectUpdate);
			
			//update.set("name",null == client.getClientName() ? _clientUpdate.getClientName():client.getClientName());
			/*update.set("description",null == client.getDescription() ? _clientUpdate.getDescription():client.getDescription());
			update.set("note",null == client.getNote() ? _clientUpdate.getNote():client.getNote());
			update.set("hours",null == client.getHours() ? _clientUpdate.getHours() : client.getHours());*/
			
			mongoTemplate.updateFirst(query, update, this.entityType);
			
			subject = findById(id);
		}
		return subject;
	}
	
	protected abstract void populateUpdate(Update update, T subject, T subjectUpdate);
	

	@Override
	public T removeById(String id) {
		T subject = findById(id);
		if(subject!=null)
			mongoTemplate.remove(subject);
		return subject;
	}

	@Override
	public List<T> findByDescription(String name) {
		Query query = query(where("description").is(name));
		return mongoTemplate.find(query, this.entityType);
	}

	public Class<T> getEntityType() {
		return entityType;
	}

	public MongoOperations getMongoTemplate() {
		return mongoTemplate;
	}

}
