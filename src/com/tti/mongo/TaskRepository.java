package com.tti.mongo;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.tti.dao.TaskDAO;
import com.tti.model.Task;

@Repository("mongoTaskDAO")
public class TaskRepository extends TtiRepository<Task> implements TaskDAO {

	@Override
	protected void populateUpdate(Update update, Task subject, Task subjectUpdate) {
		update.set("description",null == subject.getDescription() ? subjectUpdate.getDescription():subject.getDescription());
		update.set("note",null == subject.getNote() ? subjectUpdate.getNote():subject.getNote());
		update.set("hours",null == subject.getHours() ? subjectUpdate.getHours() : subject.getHours());		
		update.set("name",null == subject.getName() ? subjectUpdate.getName() : subject.getName());		
		update.set("clientId",null == subject.getClientId() ? subjectUpdate.getClientId() : subject.getClientId());		
	}
	
	@Override
	public List<Task> findTasksByClientId(String id) {
		Query query = query(where("clientId").is(id));
		return this.getMongoTemplate().find(query, this.getEntityType());
	}
	

}
